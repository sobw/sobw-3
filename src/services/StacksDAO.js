import DataAdapter from './dataadapter';

const StacksDAO = {
  getAll: async function () {
    return await DataAdapter.execute('GET', 'http://localhost:3000/v1/stacks');
  },
  getById: async function () {},
  create: async function () {},
  update: async function () {},
  delete: async function () {},
};

export default StacksDAO;
