import axios from 'axios';

const execute = async function (method, url, data = undefined) {
  return await axios({ method, url, data });
};

export default {
  execute,
};
